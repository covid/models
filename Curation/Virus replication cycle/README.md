# SARS CoV-2 Virus replication cycle

## Map description
This map describes the SARS-CoV-2 replication cycle that is comprised of the following sequential events: virus attachment, entry, transcription, replication, assembly and release to extracellular space (ES). Only the virus attachment and entry processes shown in this map were from papers reporting findings directly related to SARS-CoV-2 [PMID 32142651, PMID 32094589, PMID 32047258, PMID 32362314]; the remaining processes were inferred based on papers reporting findings related to other coronaviruses [PMID 11907209, PMID 15564471, PMID 17316733. PMID 21203998, PMID 22438542, PMID 23035226, PMID 23943763, PMID 27712623, PMID 28484023, PMID 28720894, PMID 30632963, PMID 31226023, PMID 8830530, DOI 10.1101/2020.03.24.005298].

To attach to host cells, SARS-CoV-2 uses its envelope glycoprotein spike (S) that binds at least to one cellular membrane receptor, the angiotensin-converting enzyme 2 (ACE2) [PMID 32094589, PMID 32142651]. Once attached, SARS-CoV-2 can enter cells either by direct fusion of the virion and cell membranes in the presence of proteases transmembrane protease, serine 2 (TMPRSS2) and FURIN [PMID 32142651, PMID 32362314] or by endocytosis in the absence of these proteases [PMID 32142651]. Regardless of the entry mechanism, the S protein has to be activated to initiate the plasma or endosome membrane fusion process. While in the cell membrane S is activated by TMPRSS2 and FURIN [PMID 32142651, PMID 32362314], in the endosome S is activated by cathepsins B (CTSB) and L (CTSL) [PMID 32142651]. When activated, S promotes the cell or endosome membrane fusion [PMID 32047258] with the virion membrane and then the nucleocapsid is injected into the cytoplasm.

Within host cell, SARS-CoV-2 is expected to hijack the rough endoplasmic reticulum (RER)-linked host translational machinery that then synthesize the viral proteins replicase polyprotein 1a (pp1a) and replicase polyprotein 1ab (pp1ab) directly from the virus (+)genomic RNA (gRNA) [PMID 31226023, PMID 27712623]. Through a complex cascade of proteolytic cleavages, pp1a and pp1ab give raise to 16 non-structural proteins (Nsps) [PMID 11907209, PMID 15564471, PMID 21203998]. Most of these Nsps collectively form the replication transcription complex (RTC) that is anchored to the membrane of the double-membrane vesicle (DMV) [PMID 11907209, PMID 30632963], a coronavirus replication organelle induced by Nsps 3, 4 and 6 [PMID 23943763].

RTC is thought to play two main roles: (1) to trigger the synthesis of both (+)gRNA and (+)subgenomic messenger RNAs (sgmRNAs) via negative-stranded templates [PMID 8830530, PMID 22438542, DOI 10.1101/2020.03.24.005298] and (2) to protect intermediate double-stranded RNAs from the cell innate immunity sensors [PMID 17316733]. The (+)sgmRNAs are translated by the RER-attached translation machinery into structural (E, M, N and S) and accessory proteins (Orf3a, Orf6, Orf7a, Orf7b, Orf8, Orf9b, Orf10 and Orf14) [PMID 27712623, PMID 31226023]. The structural proteins and the newly generated (+)gRNAs are assembled into new virions in the endoplasmic reticulum-Golgi intermediate compartment and these virions are released to ES via smooth-walled vesicles [PMID 31226023].  

## Cross-talk with other pathways (in the map, and in general)
To understand how COVID-19 develops, it is crucial to decipher the underlying molecular mechanisms of the SARS-CoV-2 replication cycle since all signalling cascades that culminate in the emergence of clinical symptoms derive essentially from the events encompassing the replication cycle. So, as expected, all other pathways described in this COVID-19 disease map are downstream to the replication cycle. It is worth noting, however, that two pathways, ER stress and linoleic acid metabolism, are apparently disconnected from the virus replication cycle.

## Creators
[Marcio Acencio](https://fairdomhub.org/people/1518)
[Alexander Mazein](https://fairdomhub.org/people/755)

## Contributors and reviewers
[Marek Ostaszewski](https://fairdomhub.org/people/665)

## Model at FairdomHub (https://fairdomhub.org/models/703)
