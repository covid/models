# SARS-CoV 2 and Electron Transport Chain

## Map description

The primary functions of mitochondria are the production of ATP, the energy currency of the cell and to regulate cellular metabolism [DOI 10.1186/s12915-015-0201-x]. It is affected by SARS-CoV-2 on several levels [DOI 10.1101/2020.03.22.002386]. The mammalian mitochondrial electron transport chain (ETC) consists of in the inner membrane embedded complexes I-IV, and the electron transporters cytochrome c and ubiquinone [DOI 10.3892/ijmm.2019.4188]. the oxidative phosphorylation system (OXPHOS) complex comprise the ETC and complex V. Each complex consists of catalytic activity performing core proteins and a large number of subunits supporting assembly, regulation and stability. Complex assembly is further supported by OXPHOS factors which are encoded by both mitochondrial and nuclear DNA [PMID: 30030361].

The ETC generates a proton gradient across the mitochondrial inner membrane, which is used by complex V to produce ATP. Some electrons are transferred to O2, causing the generation of reactive oxygen species (ROS) [DOI 10.3892/ijmm.2019.4188]. Complex I is also involved in superoxide production via paraquate [PMID: 18039652].

SARS-CoV-2 open reading frame 9c (Orf9c) interacts with the complex I subunits NDUFB9 and NDUFA1 and the OXPHOS factors ACAD9 and ECSIT, while non-structural protein (Nsp) 7 interacts with NDUFAF7 [DOI 10.1101/2020.03.22.002386].

Mitochondrial ribonucleoprotein biogenesis is inhibited by Nsp8’s interaction with the mitochondrial ribosomal proteins MRPS5, MRPS27, MRPS2, and MRPS25. tRNA methyltransferase 1 (TRMT1), which modulates mitochondrial tRNA [PMID: 28752201] interacts with nsp5, possibly confining TRMT1 to the mitochondrion [DOI 10.1101/2020.03.22.002386].

Mitochondrial protein import is inhibited by both Nsp4 and Orf9b. The translocase of the inner membrane (TIM) complexes and translocase of the outer membrane (TOM) complex support the transport of protein precursors into the mitochondrial matrix and the mitochondrial membrane [10.1146/annurev.cellbio.13.1.25]. While Nsp4 interacts with TIM subunits TIM9 and TIM10,
Orf9b interacts with mitochondrial import receptor Tom70 [DOI: 10.1101/2020.03.22.002386v3], which is not only involved in mitochondrial protein transport but also in IRF-3 activation [DOI: 10.1038/cr.2010.103].


## Cross-talk with other pathways (in the map, and in general)

The map contains the ETC, complex V including OXPHOS factors, mitochondrial ROS production, mitochondrial transmembrane transport (TIM and TOM complexes) of proteins, and mitochondrial DNA transcription and translation. Not included are Apoptosis, Necrosis, and a detailed network of the citric acid cycle (TCA) with its connections to glycolysis and fatty acid biosynthesis.

It has been shown that within the mitochondrion, SARS-CoV-2 subunits predominantly interact with complex I subunits [Doi 10.1101/2020.03.22.002386], which may interrupt the mitochondrial membrane potential, mitochondrial reactive oxygen species (ROS) production, and the electron transport chain [PMID: 18039652, PMID: 26770107]. Complex I inhibition is further associated with dopaminergic cell death [PMID: 2154550].

ROS play an important role in cell proliferation, cell differentiation, and adaptation to hypoxia via redox sensitive pathways. Incorrect ROS production can lead to irreversible mitochondrial oxidative damage, cell damage, and cell death [Doi 10.1016/j.pneurobio.2011.01.007].

Considering that out of the 1500 different proteins within the mitochondrion 99% are encoded by the nuclear genome and transferred into the mitochondrion via TOM and TIM [PMID: 30405116], a dysfunction of said complexes leads to further inhibition of complex I and mitochondrial function in general. As mitochondria are present in all mammalian cells, these effects can present anywhere within the system, in fact mitochondrial dysfunction has been associated with excessive fatigue and various chronic diseases [PMID: 26770107].

## Creators
- [Julia Scheel](https://fairdomhub.org/people/1686)

## Contributors and reviewers
- [Shailendra Gupta](https://fairdomhub.org/people/1641)
- [Matti Hoch](https://fairdomhub.org/people/1520)
- [Marek Ostaszewski](https://fairdomhub.org/people/665)

## Model at FairdomHub (https://fairdomhub.org/models/719)
