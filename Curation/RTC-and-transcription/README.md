# SARS-CoV-2 mechanisms of transcription and RTC

## Map description
The map shows a key mechanism of the viral life cycle, the viral gene expression of proteins involved in genome replication and the production of the viral proteins. The initial step in this process is the creation of the viral replication transcription complex (RTC). After expression of pp1a and pp1ab [PMID 18798692] the two proteins are split into 16 nonstructural proteins (Nsps) [PMID 16928755] that form the RTC [PMID 24348241, PMID 16228002]. Nsp3, Nsp4 and Nsp6 induce the formation of double membrane vesicles (DMVs) that are built from ER membranes [PMID23943763]. Inside the DMVs the genomic RNA (gRNA) gets replicated, new RTCs are built and subgenomic RNAs (sgRNAs) are transcribed [PMID 18451981]. The sgRNAs are translated into viral accessory and structural proteins.

## Cross-talk with other pathways (in the map, and in general)

The transcription and formation of the RTC is a central process in the viral replication cycle.  After the nucleocapsid enters the host cell cytoplasm the Sars-CoV-2 RNA is released and the RTC is built. After the transcription of the genomic RNA (gRNA) as well as the subgenomic RNA (sgRNA) the viral proteins are translated. New nucleocapsids are packed, enveloped by a membrane, and released from the host cell.

Using the ER membrane as transcription and replication network is a common strategy in single stranded RNA (ssRNA) viruses that is thought to shield the virus from the host immune system [PMID 18798692], [PMID 18451981]. The two Toll-like receptors TLR7 and TLR8 recognize ssRNA and induce an innate immune signaling cascade [PMID 22258243] (PAMP signaling pathway). The Nsps interact with multiple other pathways: Nsp9 interactions, Nsp14 and metabolism, Nsp4 and Nsp6 interactions and the Interferon1 pathway (Nsp3).

## Creators
- [Hanna Borlinghaus](https://fairdomhub.org/people/1597)
- [Tobias Czauderna](https://fairdomhub.org/people/1559)
- [Falk Schreiber](https://fairdomhub.org/people/1533)

## Model at FairdomHub (https://fairdomhub.org/models/704)
