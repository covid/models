# The impact of SARS CoV-2 Nsp14 on metabolism

## Map description
The viral protein Nsp14 of the novel SARS-CoV-2 is known to interact with three human proteins: the ⍺-galactosidase A (GLA), sirtuin 5 (SIRT5), and the IMP dehydrogenase 2 (IMPDH2) [DOI 10.1038/s41586-020-2286-9]. GLA plays a central role in the galactose metabolism and catalyzes the hydrolysis of ⍺-D-galactopyranose residues [DOI 10.1016/j.jmb.2004.01.035]. SIRT5 is a NAD-dependent desuccinylase and demanlonylase [DOI 10.1126/science.1207861]. It is located in the mitochondria and plays a pivotal role in the oxidative metabolism and apoptosis initiation [DOI 10.1016/j.jmb.2008.07.048]. In liver cells, it controls the detoxification of ammonia [DOI 10.1080/15548627.2015.1009778]. It is furthermore associated with the human nicotinate and nicotinamide metabolism. The IMP dehydrogenase is the rate-limiting enzyme in the de novo synthesis of GTP [DOI 10.1186/s13008-018-0038-0]. This feature turns the IMPDH2 into an attractive target for the regulation of the purine metabolism. The purine metabolism contains potential antiviral-targets, including the IMPDH2, the guanylate kinase (GK1), and the adenylosuccinate lyase (ADSL) [DOI 10.5281/zenodo.3752641].

The map displays the three pathways (galactose metabolism, nicotinate and nicotinamide metabolism, and purine metabolism) that are affected by Nsp14. The modulating nature of Nsp14 on the three enzymes is currently not known.

## Cross-talk with other pathways
The galactose metabolism, harboring the GLA enzyme, is interconnected with the amino sugar and nucleotide sugar metabolism, as well as further sugar metabolisms, such as fructose or mannose. The enzyme SIRT5 can desuccinylate the serine hydroxymethyltransferase SHMT2, leading to increased serine catabolism [DOI 10.1158/0008-5472.CAN-17-1912]. The IMPDH plays a crucial role in the metabolism and proliferation of cells [DOI 10.1186/s13008-018-0038-0]. It is a central human metabolism, connected to a variety of other pathways, such as the riboflavin or histidine metabolism, or the pentose phosphate pathway. Downstream of the purine metabolism, the nucelobases guanine and adenine are synthesized.

## Creators
[Alina Renz](https://fairdomhub.org/people/1564)
[Andreas Draeger](https://fairdomhub.org/people/1569)

## Contributors and reviewers
[Marek Ostaszewski](https://fairdomhub.org/people/665)

## Model at FairdomHub (https://fairdomhub.org/models/710)

