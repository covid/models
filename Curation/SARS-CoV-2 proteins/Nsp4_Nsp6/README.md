# SARS CoV-2 Nsp4 and Nsp6 interactions with human and viral proteins

## Map description
The map is centred on the interactions of Nsp4 and Nsp6 proteins and the host human proteins. Nsp4 and Nsp6 are part of the 16 non-structural proteins (Nsp1-16) that form the replicase/transcriptase complex (RTC) [PMID 32353859]. The RTC consists of multiple enzymes, notably including the papain-like protease (Nsp3) with whom both Nsp4 and Nsp6 interact to induce rearrangement of the cellular membrane to form double-membrane vesicles or spherules, where the coronavirus replication transcription complex (RTC) is assembled and anchored in SARS-COV-1 [PMID 31226023].
Nsp4 interacts with several proteins (TIMM9, 10, 10B and 29) of the TIM complex, the mitochondrial transporters that regulates import of transmembrane proteins into the inner mitochondrial membrane [PMID 32353859]. Nsp4 also interacts with: ALG11, a mannosyltransferase involved in the synthesis of core oligosaccharide on the cytoplasmic face of the endoplasmic reticulum [DOI 10.1093/hmg/ddq016]; IDE, a zinc metallopeptidase that degrades intracellular insulin [DOI 10.1210/mend-4-8-1125] involved in antigen presentation via MHC class I; NUP210, a nucleoporin essential for nuclear pore assembly and fusion, [DOI 10.1091/mbc.e03-04-0260]; and DNAJC11, required for mitochondrial inner membrane organization [PMID 25997101].
Nsp6, whose alleged function is limiting autophagasome expansion [PMID 32353859], interacts with several ATPases that are considered located in different cellular membranes: ATP5MG, mitochondrial ATP synthase membrane subunit g [PMID 11230166]; ATP6AP1, subunit of the vacuolar ATP synthase protein pump [PMID 27231034]; and ATP13A3, a probable cation-transporting ATPase 13A3 allegedly in the cytoplasmic membrane [PMID 11867234]. Additionally, Nsp6 also interacts with SIGMAR1, which functions in lipid transport from the endoplasmic reticulum and is involved in a wide array of cellular functions probably through regulation of the biogenesis of lipid microdomains at the plasma membrane [DOI 10.1074/jbc.272.43.27107]; and also with a member of the solute carrier superfamily SLC6A15 although this interaction fell below the authors’ scoring thresholds [PMID 32353859]. Interestingly, SLC6A15 also interacts with SARS-CoV-2 proteins Orf9c and M, even though below the authors’ scoring thresholds [PMID 32353859].

No tissue or cell-type specificity that we know of, even though most functional descriptions were taken from SARS-CoV-1.  

## Cross-talk with other pathways (in the map, and in general)
The map is located at the level of the functions of Nsp4 and Nsp6. On the horizontal dimension, these proteins interact together with Nsp3 to rearrange the plasma membrane into double-membrane vesicles. Additionally, Nsp6 has common human proteins targets with Orf9c and M SARS-CoV-2 viral proteins.
On the vertical dimension, these interactions are downstream the entering, decapsulation and translation of the viral RNAs (we have depicted the latter, but just as placeholders) and they are upstream of the different pathways from all the human proteins connected by those. These would mainly be mitochondrial transport through the TIM complex for Nsp4 and different ion pumping and oxidative phosphorylation pathways for Nsp6.

This map will connect with these other maps from the consortium: Virus replication cycle, RTC and transcription, PAMP signalling (as Orf9 and M proteins affect it) and HCoVs autophagy (for Nsp6).

## Creators
* [Arnau Montgud](https://fairdomhub.org/people/1537)
* [Miguel Ponce de Leon](https://fairdomhub.org/people/1536)

## Contributors and reviewers
* [Alfonso Valencia](https://fairdomhub.org/people/61)

## Model at FairdomHub (https://fairdomhub.org/models/708)
