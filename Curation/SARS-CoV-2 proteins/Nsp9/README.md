# SARS CoV-2 Nsp9 binding proteins

## Map description
The non-structural protein 9 (Nsp9) has not yet identified its designated function but is estimated that it is essential forviral RNA synthesis for both transcription and translation. We included the binding proteins of Nsp9 in this map [1] with further functional partners especially relating with transcription and translation. We also involved the other Nsps which are suggested to bind with Nsp9 in recent literatures, and the binding proteins of those other Nsps. We chose the proteins which bind poly-RNA, and work for transportation between nucleus and cytoplasm. We also added the other virus-relating processes which those binding proteins are reported their contribution. The map contains the medicines which affect to the proteins in the map.

### How they are related to the COVID-19
These processes may contribute for the COVID-19 replication by recruiting the working complexes via Nsp9 and the other Nsps [2]. In this scenario, the complex with Nsp8 may play significant role. Nsp9 and 10 complex is a candidate which can activate immune system of patients via neutrophil. The complex with Nsp14 may work transportation between nucleus and cytoplasm. There exist the other several possibilities that Nsp9 works more as a competitive/uncompetitive/noncompetitive inhibitor of the binding proteins than a cooperator. We may test and predict this possibility by simulating the time-course fluctuation of the reactants among possible several scenarios.

### What are the key molecular mechanisms
Transcription factors which directly bind Nsp9 may have significant role to replicate viral RNA. Another COVID-19 protein Nsp8 is expected to cooperate with Nsp9 for RNA replication [3]. Some of the binding proteins are known which cooperate with the other viral proteins for such as recruiting virus proteins into cellular nuclei, etc. These processes are expected as the part of critical function of Nsp9, but have not yet confirmed with experiments.
There exist at least two therapeutic candidates of medicines in this map; one is Remdesivir, which was found originally for Ebola, now is supposed to work against Nsps complex, and Ribavirin[4], which was a candidate medicine for HCV infection, and is supposed to inhibit nucleus-cytoplasm transporting process.

### What is the physiological context
The processes in this map are about general function of a cell.
All the processes in this map are composed with the proteins in human cells.

### References
[1] Gordon DE, et al. A SARS-CoV-2 Protein Interaction Map Reveals Targets for Drug Repurposing. Nature. 2020 Apr 30. doi: 10.1038/s41586-020-2286-9. PMID: 32353859.
[2] Jingjiao Li, Mingquan Guo, Xiaoxu Tian, Chengrong Liu, Xin Wang, Xing Yang, Ping Wu, Zixuan Xiao, Yafei Qu, Yue Yin, Joyce Fu, Zhaoqin Zhu, Zhenshan Liu, Chao Peng, Tongyu Zhu, Qiming Liang. Virus-host interactome and proteomic survey of PMBCs from COVID-19 patients reveal potential virulence factors influencing SARS-CoV-2 pathogenesis.
bioRxiv 2020.03.31.019216; doi: https://doi.org/10.1101/2020.03.31.019216
[3] Shengjie Dong, Jiachen Sun, Zhuo Mao, Lu Wang, Yi-Lin Lu, Jiesen Li. A Guideline for Homology Modeling of the Proteins From Newly Discovered Betacoronavirus, 2019 Novel Coronavirus (2019-nCoV). J Med Virol. 2020 Mar 17;10.1002/jmv.25768. doi: 10.1002/jmv.25768, PMID: 32181901.
[4] Ivan Fan-Ngai Hung, Kwok-Cheung Lung, Eugene Yuk-Keung Tso, Raymond Liu, Tom Wai-Hin Chung, Man-Yee Chu, et al. Triple combination of interferon beta-1b, lopinavir–ritonavir, and ribavirin in the treatment of patients admitted to hospital with COVID-19: an open-label, randomised, phase 2 trial. The Lancet, VOL 395, ISSUE 10238, P1695-1704, MAY 30, 2020. DOI:https://doi.org/10.1016/S0140-6736(20)31042-4

## Cross-talk with other pathways
This map connect with the following diagrams.
SARS-CoV-2 and COVID-19 Pathway, nsp9/nsp10-mediated SARS-CoV-2 pathogenesis in Wiki Pathway, Nsp9 is involved the processes of viral proteins replication in a Human cell, homo-dimer formation, and heterodimerization with Nsp10.

In Reactome SARS-CoV-2 Infection also include Nsp9 dimerization process.

In individual diagrams, RTC and transcription map, Virus replication cycle map include Nsp9 and the other Nsps(Nsp7, 8, 10, 12, 14, and 16). Additionally to the above two maps, ETC map has overlaps via Nsp7 and Nsp8. Nsp14 and metabolism map overlaps via Nsp14.


## Creators
* [Noriko Hiroi](https://fairdomhub.org/people/1534)
* [Yusuke Hiki](https://fairdomhub.org/people/1655)
* [Takahiro G. Yamada](https://fairdomhub.org/people/1645)
* [Akira Funahashi](https://fairdomhub.org/people/1561)

## Model at FairdomHub (https://fairdomhub.org/models/736)
