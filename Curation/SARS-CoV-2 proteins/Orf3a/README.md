# SARS-CoV-2 Orf3a protein interactions

## Map description
ORF3a is the largest of the SARS-CoV accessory proteins at 274 amino acids in length with a native molecular weight of 31 kDa[1]. Orf3a with HOPS complex modify endomembrane compartments to favor coronavirus replication[2]IRF3, IL1A and TNFRSF1A receptor complex activate another complex which is containing IKBKG, IKBKB, and CHUK. For the activation of ininflammasome SARS-CoV unregulated the expression of pro-IL-1β transcription the result NF-κB activates. Two mechanisms introduce firstly Orf3a, and E proteins, leading to NLRP3 inflammasome assembly. ORF3a promotes ASC ubiquitination and consequent assembly of inflammasome.  Second Orf8b interacts activates NLRP3 leads to proteolytic cleavage of pro-caspase 1 and pro-IL-1β[3]. TRAF3 also play a vital role to activate ininflammasome Orf3a interacts with TRAF3 to activate NF-kB, resulting in transcription of IL-1b. ORF3a interacts with TRAF3 to promote ASC ubiquitination, leading to activation of caspase 1 and IL-1b [4].TRIM59 regulate antiviral innate immune signaling and are usurped by Orf3a. TRIM59 negatively regulates NF-κB and IRF-3/7-mediated signal pathways[2]. TRAF6, activating protein (TAB) 2/3 recruits the critical kinase TAK1 to enable the active of NF-_kB essential modulator , the regulatory domain of inhibitor of NF-kB . Tripartite motifs (TRIMs) play an integral role in the positive and negative regulation of antiviral pathways[5].

## Cross-talk with other pathways
Trim59 pathway cross talk with other TRIM pathways used to inhibit the Nf-kb response in Orf3a COVID-19 Disease map.

TLR3, 7, 8 and 9, detect viral RNA and DNA in the endosome, whereas RLRs bind to viral RNA in the cytoplasm[6]. TLR7, TLR8 and TLR9, induces antiviral responses by producing interferon-α (IFN-α). Production of IFN-α is dependent on the Toll–interleukin-1 receptor domain–containing adaptor MyD88.[7] TRAF3 is required for TRIF-dependent activation. Internalized TLR4 recruits TRAF3 and TRAF6 to active endosomes via TRIF. TRAF6 mediates MyD88- and TRIF-induced activation of NF-κB1. TRAF6 bind to TAB2/3 to activate to NEMO to activate IKKα/β in the receptor complex[8]. TRIM23 knockdown decreased IFNβ production with SV infection in the cell.TRIM23-NEMO complexes is important for antiviral responses[9]. TRIM38 as a negative feedback regulator for TLR-induced production of proinflammatory cytokines by targeting TRAF6[10]. TRIM30α promoted the degradation of TAB2 and TAB3 and inhibited NF-κB activation induced by TLR signaling[11]. TRIM29 inhibited interferon-regulatory factors and signaling via the transcription factor NF-κB by degrading the adaptor NEMO and that TRIM29 directly bound NEMO and subsequently induced its ubiquitination and proteolytic degradation[12]. TRIM22 inhibits Tab2/3 complex independently of its E3 ubiquitin ligase activity and NF-κB responsive long terminal repeat elements[13]. TRIM27 inhibits VSV infection-induced type I IFN production by promoting TBK1 degradation[14]. Overexpression of TRIM21 inhibited the activity of the IFNB1[15]. TRIM14 as a Positive Regulator of the Type I IFN Response[16]. βTrCP E3 complex and targeted for ubiquitin-mediated proteasomal degradation, releasing the NF-κB[17]. TRIM39 negatively regulates the NFκB-mediated signaling pathway through stabilization of Cactin[18]

## References
[1]	Molecular Biology of the SARS-Coronavirus. .
[2]	“A SARS-CoV-2-Human Protein-Protein Interaction Map Reveals Drug Targets and Potential DrugRepurposing.”
[3]	“A Tug-Of-War Between Severe Acute Respiratory Syndrome Coronavirus 2 and Host Antiviral Defence: Lessons From Other Pathogenic Viruses.”
[4]	“Siu, Kam-Leung et al. ‘Severe acute respiratory syndrome coronavirus ORF3a protein activates the NLRP3 inflammasome by promoting TRAF3-dependent ubiquitination of ASC.’ FASEB journal : official publication of the Federation of American Societies for Experimental Biology vol. 33,8 (2019): 8865-8877. doi:10.1096/fj.201802418R.”
[5]	“The TRIMendous Role of TRIMs in Virus–Host Interactions.”
[6]	“MAVS Forms Functional Prion-Like Aggregates To Activate and Propagate Antiviral Innate Immune Response.”
[7]	“Interferon-α induction through Toll-like receptors involves a direct interaction of IRF7 with MyD88 and TRAF6.”
[8]	“TRAF molecules in cell signaling and in human diseases.”
[9]	“Polyubiquitin Conjugation to NEMO by Triparite Motif Protein 23 (TRIM23) Is Critical in Antiviral Defense.”
[10]	“Tripartite Motif-Containing Protein 38 Negatively Regulates TLR3/4- And RIG-I-mediated IFN-β Production and Antiviral Response by Targeting NAP1.”
[11]	“TRIM30α negatively regulates TLR-mediated NF-κB activation by targeting TAB2 and TAB3 for degradation.”
[12]	“Identification of a Role for TRIM29 in the Control of Innate Immunity in the Respiratory Tract.”
[13]	“TRIM22 Inhibits Influenza A Virus Infection by Targeting the Viral Nucleoprotein for Degradation.”
[14]	“Zheng Q, Hou J, Zhou Y, Yang Y, Xie B, Cao X. Siglec1 suppresses antiviral innate immune response by inducing TBK1 degradation via the ubiquitin ligase TRIM27. Cell Res. 2015;25(10):1121-1136. doi:10.1038/cr.2015.108.”
[15]	“Zhang Z, Bao M, Lu N, Weng L, Yuan B, Liu YJ. The E3 ubiquitin ligase TRIM21 negatively regulates the innate immune response to intracellular double-stranded DNA. Nat Immunol. 2013;14(2):172-178. doi:10.1038/ni.2492.”
[16]	“Zhou Z, Jia X, Xue Q, et al. TRIM14 is a mitochondrial adaptor that facilitates retinoic acid-inducible gene-I-like receptor-mediated innate immune response. Proc Natl Acad Sci U S A. 2014;111(2):E245-E254. doi:10.1073/pnas.1316941111.”
[17]	“Liu S, Chen ZJ. Expanding role of ubiquitination in NF-κB signaling. Cell Res. 2011;21(1):6-21. doi:10.1038/cr.2010.170.”
[18]	“Christian F, Smith EL, Carmody RJ. The Regulation of NF-κB Subunits by Phosphorylation. Cells. 2016;5(1):12. Published 2016 Mar 18. doi:10.3390/cells5010012.”


## Creators
* [Muhammad Naveez](https://fairdomhub.org/people/1546)

## Model at FairdomHub (https://fairdomhub.org/models/720)
