# SARS CoV-2 E protein mechanisms

## Map description
This map focuses on interaction mechanisms between Envelope (E) protein and host proteins in different pathways. To allow a more detailed description of every possible phenotypes, E protein of both SARS-CoV-2 and SARS-CoV were used to built this diagram. In fact, E proteins (ORF4) from SARS CoV and SARS CoV-2 had a sequence identity 94.7% [10.1007/s10930-020-09901-4]. The activity of E protein was described in two compartments: in the nucleus and on  plasmatic membrane. In the nucleus, strong inhibiting interactions were described between E protein with bromodomain proteins, BRD2 and BRD4 [10.1038/s41586-020-2286-9]. both bromodomain proteins were associated with acetylating of H3C15 histone protein to modulate chromatin accessibility [10.1038/nsmb.3228.; 10.1016/j.molcel.2008.01.018]. Moreover BRD4 plays an important role in the transcription, activating P-TEFb complex (Cyclin T1: CDK9) that phosphoriles RNA polymerase II [10.1038/emboj.2008.121]. On other hand, BRD2 allows to activate the TBP, transcription factor that start the gene transcription [10.1007/s11010-006-9223-6]. The inhibiting activity of E protein could acted negatively on c ell responce against viral infection.

On plasmatic membrane, the E protein interacts with PALS1 of human epithelial cells during SARS-CoV infection [10.1091/mbc.E10–04–0338].  PALS1 represents one of components of CRB3–Pals1–PATJ complex, the second major cell polarity protein complex at Tight Junctions [10.1146/annurev.cellbio.22.010305.104219]. Loss of PALS1 and the disruption of Tight Junctions allowed the progressive infiltration of SARS-CoV virions [10.1091/mbc.E10–04–0338]. Finally, E protein interacted negatively with  two proteins involved in maintaining ionic homeostasis, Na+/K+ ATPase α-1 subunit and STOML3, which could reduce levels and activity of human epithelial sodium channels [10.1016/j.virol.2011.03.029].  

## Cross-talk with other pathways (in the map, and in general)
Up to now, E protein involved following pathways: Chromatin organization, Gene transcription, Cell junction and Ion transport. In Apoptosis map reported in COVID19 Disease Maps, an important inhibiting interaction between anti-apoptotic protein B-cell lymphoma-extra-large (Bcl-xL) protein and E protein has been reported [10.1042/BJ20050698]. This inhibiting, enhanced by ORF7a, allows to activate Bax pathways inducing apoptotic cell death. Other diagram, where E protein plays a important role, is viral replication cycle. This diagram explained many steps of viral replication cycle, where E protein involved [10.1002/jmv.25693; 10.1080/22221751.2020.1719902].

## Creators
[Francesco Messina](https://fairdomhub.org/people/1592)

## Contributors and reviewers
[Francesco Nicola Lauria](https://fairdomhub.org/people/1593)
[Emanuela Giombini](https://fairdomhub.org/people/1588)

## Model at FairdomHub (https://fairdomhub.org/models/721)
