# COVID-19 Disease Map biocuration roadmap

COVID-19 Disease Map pathways span a range of currently known host-cell virus interactions and mechanisms. Nevertheless, certain aspects of the disease are not represented in detail, particularly cell-type-specific immune response, and susceptibility features. Their mechanistic description is of great importance, as suggested by clinical reports on the involvement of these pathways in the molecular pathophysiology of the disease. The mechanisms outlined below will be the next targets in our curation roadmap. 

## Cell type-specific immune response
COVID-19 causes serious disbalance in multiple populations of immune cells, including peripheral CD4+ and CD8+ cytotoxic T lymphocytes, B cells and NK cells [1–6]. This may be the result of functional exhaustion due to SARS-CoV-2 S protein and excessive pro-inflammatory cytokine response [2,7], promoted by an abnormal increase of the Th17:Treg cell ratio [8]. Moreover, the ratio of naive-to-memory helper T-cells increases while the level of T regulatory cells decreases in severe cases [9]. Pulmonary recruitment of lymphocytes into the airways, including Th17 and cytotoxic CD8+ T-cells [10], may explain this imbalance and the increased neutrophil‐lymphocyte ratio in peripheral blood [1,11,12]. To address this aspect of the disease we plan to implement cell type representations of different populations, and encode their cell surface receptors and transition mechanisms. With the help of single-cell omics profiling, we plan to adapt these to reflect COVID-19 specificity. 

## Susceptibility features of the host

SARS-CoV-2 infection is associated with increased morbidity and mortality in individuals with underlying medical conditions, chronic diseases or a compromised immune system [13–16]. Groups at risk are men, pregnant and postpartum women, and individuals with high occupational viral exposure [17–19]. Other susceptibility factors include the ABO blood groups [20–27] and respiratory conditions [28–33].
Importantly, age is one of the key aspects contributing to the severity of the disease [15,34]. Age-related elevated levels of inflammation [34–37], immunosenescence and cellular stress of ageing cells [15,34,38–40] may contribute to the risk. In contrast, children are generally less likely to develop severe disease [41,42], with the exception of infants [38,43–45]. However, some previously healthy children and adolescents can develop a multisystem inflammatory syndrome following SARS-CoV-2 infection [46–50]. 

Finally, several genetic factors have been proposed and identified to influence susceptibility and severity, including the ACE2 gene, HLA locus, errors influencing type I IFN production, TLR pathways, myeloid compartments, as well as cytokine polymorphisms [23,51–58].
Connecting the susceptibility features to specific molecular mechanisms will allow us to better understand the contributing factors. These features can be directly incorporated as elements of relevant diagrams. Another possibility is connecting the diagrams of the Map to clinical and phenotypic data following big data workflows as demonstrated in other settings [59,60]. This can lead to a series of testable hypotheses, including the role of lipidomic reprogramming [61,62] or of vitamin D [63–65] in modifying the severity of the disease. Another testable hypothesis is that the immune phenotype associated with asthma inhibits pro-inflammatory cytokine production and modifies gene expression in the airway epithelium, protecting against severe COVID-19 [32,33,66].


## Bibliography

1. Liu J, Li S, Liu J, Liang B, Wang X, Wang H, et al. Longitudinal characteristics of lymphocyte responses and cytokine profiles in the peripheral blood of SARS-CoV-2 infected patients. EBioMedicine. 2020;55:102763. 
2. Zheng M, Gao Y, Wang G, Song G, Liu S, Sun D, et al. Functional exhaustion of antiviral lymphocytes in COVID-19 patients. Cell Mol Immunol. 2020;17:533–5. 
3. Tan M, Liu Y, Zhou R, Deng X, Li F, Liang K, et al. Immunopathological characteristics of coronavirus disease 2019 cases in Guangzhou, China. Immunology. 2020;160:261–8. 
4. Ni L, Ye F, Cheng M-L, Feng Y, Deng Y-Q, Zhao H, et al. Detection of SARS-CoV-2-Specific Humoral and Cellular Immunity in COVID-19 Convalescent Individuals. Immunity. 2020;52:971-977.e3. 
5. Lucas C, Wong P, Klein J, Castro TBR, Silva J, Sundaram M, et al. Longitudinal analyses reveal immunological misfiring in severe COVID-19. Nature. 2020;584:463–9. 
6. Su Y, Chen D, Lausted C, Yuan D, Choi J, Dai C, et al. Multiomic Immunophenotyping of COVID-19 Patients Reveals Early Infection Trajectories [Internet]. Immunology; 2020 Jul. Available from: http://biorxiv.org/lookup/doi/10.1101/2020.07.27.224063
7. Bortolotti D, Gentili V, Rizzo S, Rotola A, Rizzo R. SARS-CoV-2 Spike 1 Protein Controls Natural Killer Cell Activation via the HLA-E/NKG2A Pathway. Cells. 2020;9. 
8. Zhang Y-Y, Li B-R, Ning B-T. The Comparative Immunological Characteristics of SARS-CoV, MERS-CoV, and SARS-CoV-2 Coronavirus Infections. Front Immunol. 2020;11:2033. 
9. Qin C, Zhou L, Hu Z, Zhang S, Yang S, Tao Y, et al. Dysregulation of Immune Response in Patients With Coronavirus 2019 (COVID-19) in Wuhan, China. Clin Infect Dis Off Publ Infect Dis Soc Am. 2020;71:762–8. 
10. Xu Z, Shi L, Wang Y, Zhang J, Huang L, Zhang C, et al. Pathological findings of COVID-19 associated with acute respiratory distress syndrome. Lancet Respir Med. 2020;8:420–2. 
11. Ciccullo A, Borghetti A, Zileri Dal Verme L, Tosoni A, Lombardi F, Garcovich M, et al. Neutrophil-to-lymphocyte ratio and clinical outcome in COVID-19: a report from the Italian front line. Int J Antimicrob Agents. 2020;56:106017. 
12. Kong M, Zhang H, Cao X, Mao X, Lu Z. Higher level of neutrophil-to-lymphocyte is associated with severe COVID-19. Epidemiol Infect. 2020;148:e139. 
13. Guan W-J, Liang W-H, Zhao Y, Liang H-R, Chen Z-S, Li Y-M, et al. Comorbidity and its impact on 1590 patients with COVID-19 in China: a nationwide analysis. Eur Respir J. 2020;55. 
14. Liu D, Cui P, Zeng S, Wang S, Feng X, Xu S, et al. Risk factors for developing into critical COVID-19 patients in Wuhan, China: A multicenter, retrospective, cohort study. EClinicalMedicine. 2020;25:100471. 
15. Salimi S, Hamlyn JM. COVID-19 and Crosstalk With the Hallmarks of Aging. J Gerontol A Biol Sci Med Sci. 2020;75:e34–41. 
16. Fung M, Babik JM. COVID-19 in Immunocompromised Hosts: What We Know So Far. Clin Infect Dis Off Publ Infect Dis Soc Am. 2020; 
17. Scully EP, Haverfield J, Ursin RL, Tannenbaum C, Klein SL. Considering how biological sex impacts immune responses and COVID-19 outcomes. Nat Rev Immunol. 2020;20:442–7. 
18. Collin J, Byström E, Carnahan A, Ahrne M. Public Health Agency of Sweden’s Brief Report: Pregnant and postpartum women with severe acute respiratory syndrome coronavirus 2 infection in intensive care in Sweden. Acta Obstet Gynecol Scand. 2020;99:819–22. 
19. Nguyen LH, Drew DA, Graham MS, Joshi AD, Guo C-G, Ma W, et al. Risk of COVID-19 among front-line health-care workers and the general community: a prospective cohort study. Lancet Public Health. 2020;5:e475–83. 
20. Zhao J, Yang Y, Huang H, Li D, Gu D, Lu X, et al. Relationship between the ABO Blood Group and the COVID-19 Susceptibility. Clin Infect Dis Off Publ Infect Dis Soc Am. 2020; 
21. Li J, Wang X, Chen J, Cai Y, Deng A, Yang M. Association between ABO blood groups and risk of SARS-CoV-2 pneumonia. Br J Haematol. 2020;190:24–7. 
22. Wu Y, Feng Z, Li P, Yu Q. Relationship between ABO blood group distribution and clinical characteristics in patients with COVID-19. Clin Chim Acta Int J Clin Chem. 2020;509:220–3. 
23. Severe Covid-19 GWAS Group, Ellinghaus D, Degenhardt F, Bujanda L, Buti M, Albillos A, et al. Genomewide Association Study of Severe Covid-19 with Respiratory Failure. N Engl J Med. 2020;383:1522–34. 
24. Zaidi FZ, Zaidi ARZ, Abdullah SM, Zaidi SZA. COVID-19 and the ABO blood group connection. Transfus Apher Sci Off J World Apher Assoc Off J Eur Soc Haemapheresis. 2020;102838. 
25. O’Sullivan JM, Ward S, Fogarty H, O’Donnell JS. More on “Association between ABO blood groups and risk of SARS-CoV-2 pneumonia.” Br J Haematol. 2020;190:27–8. 
26. Dai X. ABO blood group predisposes to COVID-19 severity and cardiovascular diseases. Eur J Prev Cardiol. 2020;27:1436–7. 
27. Gérard C, Maggipinto G, Minon J-M. COVID-19 and ABO blood group: another viewpoint. Br J Haematol. 2020;190:e93–4. 
28. Abrams EM, ’t Jong GW, Yang CL. Asthma and COVID-19. CMAJ Can Med Assoc J J Assoc Medicale Can. 2020;192:E551. 
29. Jackson DJ, Busse WW, Bacharier LB, Kattan M, O’Connor GT, Wood RA, et al. Association of respiratory allergy, asthma, and expression of the SARS-CoV-2 receptor ACE2. J Allergy Clin Immunol. 2020;146:203-206.e3. 
30. Song J, Zeng M, Wang H, Qin C, Hou H-Y, Sun Z-Y, et al. Distinct effects of asthma and COPD comorbidity on disease expression and outcome in patients with COVID-19. Allergy. 2020; 
31. Dong X, Cao Y-Y, Lu X-X, Zhang J-J, Du H, Yan Y-Q, et al. Eleven faces of coronavirus disease 2019. Allergy. 2020;75:1699–709. 
32. Liu S, Zhi Y, Ying S. COVID-19 and Asthma: Reflection During the Pandemic. Clin Rev Allergy Immunol. 2020;59:78–88. 
33. Kimura H, Francisco D, Conway M, Martinez FD, Vercelli D, Polverino F, et al. Type 2 inflammation modulates ACE2 and TMPRSS2 in airway epithelial cells. J Allergy Clin Immunol. 2020;146:80-88.e8. 
34. Meftahi GH, Jangravi Z, Sahraei H, Bahari Z. The possible pathophysiology mechanism of cytokine storm in elderly adults with COVID-19 infection: the contribution of “inflame-aging.” Inflamm Res Off J Eur Histamine Res Soc Al. 2020;69:825–39. 
35. Fulop T, Larbi A, Dupuis G, Le Page A, Frost EH, Cohen AA, et al. Immunosenescence and Inflamm-Aging As Two Sides of the Same Coin: Friends or Foes? Front Immunol. 2017;8:1960. 
36. Franceschi C, Garagnani P, Parini P, Giuliani C, Santoro A. Inflammaging: a new immune-metabolic viewpoint for age-related diseases. Nat Rev Endocrinol. 2018;14:576–90. 
37. Franceschi C, Zaikin A, Gordleeva S, Ivanchenko M, Bonifazi F, Storci G, et al. Inflammaging 2018: An update and a model. Semin Immunol. 2018;40:1–5. 
38. Yuki K, Fujiogi M, Koutsogiannaki S. COVID-19 pathophysiology: A review. Clin Immunol Orlando Fla. 2020;215:108427. 
39. Aiello F, Gallo Afflitto G, Mancino R, Li J-PO, Cesareo M, Giannini C, et al. Coronavirus disease 2019 (SARS-CoV-2) and colonization of ocular tissues and secretions: a systematic review. Eye Lond Engl. 2020;34:1206–11. 
40. Ventura MT, Casciaro M, Gangemi S, Buquicchio R. Immunosenescence in aging: between immune cells depletion and cytokines up-regulation. Clin Mol Allergy CMA. 2017;15:21. 
41. Swann OV, Holden KA, Turtle L, Pollock L, Fairfield CJ, Drake TM, et al. Clinical characteristics of children and young people admitted to hospital with covid-19 in United Kingdom: prospective multicentre observational cohort study. BMJ. 2020;370:m3249. 
42. Pierce CA, Preston-Hurlburt P, Dai Y, Aschner CB, Cheshenko N, Galen B, et al. Immune responses to SARS-CoV-2 infection in hospitalized pediatric and adult patients. Sci Transl Med. 2020;12. 
43. Dhochak N, Singhal T, Kabra SK, Lodha R. Pathophysiology of COVID-19: Why Children Fare Better than Adults? Indian J Pediatr. 2020;87:537–46. 
44. Mehta NS, Mytton OT, Mullins EWS, Fowler TA, Falconer CL, Murphy OB, et al. SARS-CoV-2 (COVID-19): What do we know about children? A systematic review. Clin Infect Dis Off Publ Infect Dis Soc Am. 2020; 
45. Liguoro I, Pilotto C, Bonanni M, Ferrari ME, Pusiol A, Nocerino A, et al. SARS-COV-2 infection in children and newborns: a systematic review. Eur J Pediatr. 2020;179:1029–46. 
46. Verdoni L, Mazza A, Gervasoni A, Martelli L, Ruggeri M, Ciuffreda M, et al. An outbreak of severe Kawasaki-like disease at the Italian epicentre of the SARS-CoV-2 epidemic: an observational cohort study. Lancet Lond Engl. 2020;395:1771–8. 
47. Chiotos K, Bassiri H, Behrens EM, Blatz AM, Chang J, Diorio C, et al. Multisystem Inflammatory Syndrome in Children During the Coronavirus 2019 Pandemic: A Case Series. J Pediatr Infect Dis Soc. 2020;9:393–8. 
48. Riphagen S, Gomez X, Gonzalez-Martinez C, Wilkinson N, Theocharis P. Hyperinflammatory shock in children during COVID-19 pandemic. Lancet Lond Engl. 2020;395:1607–8. 
49. Levin M. Childhood Multisystem Inflammatory Syndrome - A New Challenge in the Pandemic. N Engl J Med. 2020;383:393–5. 
50. Feldstein LR, Rose EB, Horwitz SM, Collins JP, Newhams MM, Son MBF, et al. Multisystem Inflammatory Syndrome in U.S. Children and Adolescents. N Engl J Med. 2020;383:334–46. 
51. Li Q, Cao Z, Rahman P. Genetic variability of human angiotensin-converting enzyme 2 (hACE2) among various ethnic populations. Mol Genet Genomic Med. 2020;8:e1344. 
52. Gemmati D, Bramanti B, Serino ML, Secchiero P, Zauli G, Tisato V. COVID-19 and Individual Genetic Susceptibility/Receptivity: Role of ACE1/ACE2 Genes, Immunity, Inflammation and Coagulation. Might the Double X-chromosome in Females Be Protective against SARS-CoV-2 Compared to the Single X-Chromosome in Males? Int J Mol Sci. 2020;21. 
53. Devaux CA, Rolain J-M, Raoult D. ACE2 receptor polymorphism: Susceptibility to SARS-CoV-2, hypertension, multi-organ failure, and COVID-19 disease outcome. J Microbiol Immunol Infect Wei Mian Yu Gan Ran Za Zhi. 2020;53:425–35. 
54. Debnath M, Banerjee M, Berk M. Genetic gateways to COVID-19 infection: Implications for risk, severity, and outcomes. FASEB J Off Publ Fed Am Soc Exp Biol. 2020;34:8787–95. 
55. Englmeier L. A theory on SARS-COV-2 susceptibility: reduced TLR7-activity as a mechanistic link between men, obese and elderly. J Biol Regul Homeost Agents. 2020;34. 
56. Schulte-Schrepping J, Reusch N, Paclik D, Baßler K, Schlickeiser S, Zhang B, et al. Severe COVID-19 Is Marked by a Dysregulated Myeloid Cell Compartment. Cell. 2020;182:1419-1440.e23. 
57. Carter-Timofte ME, Jørgensen SE, Freytag MR, Thomsen MM, Brinck Andersen N-S, Al-Mousawi A, et al. Deciphering the Role of Host Genetics in Susceptibility to Severe COVID-19. Front Immunol. 2020;11:1606. 
58. Zhang Q, Bastard P, Liu Z, Le Pen J, Moncada-Velez M, Chen J, et al. Inborn errors of type I IFN immunity in patients with life-threatening COVID-19. Science. 2020; 
59. Ostaszewski M, Mazein A, Gillespie ME, Kuperstein I, Niarakis A, Hermjakob H, et al. COVID-19 Disease Map, building a computational repository of SARS-CoV-2 virus-host interaction mechanisms. Sci Data. 2020;7:136. 
60. Gu W, Yildirimman R, Van der Stuyft E, Verbeeck D, Herzinger S, Satagopam V, et al. Data and knowledge management in translational research: implementation of the eTRIKS platform for the IMI OncoTrack consortium. BMC Bioinformatics. 2019;20:164. 
61. Bruzzone C, Bizkarguenaga M, Gil-Redondo R, Diercks T, Arana E, García de Vicuña A, et al. SARS-CoV-2 Infection Dysregulates the Metabolomic and Lipidomic Profiles of Serum. iScience. 2020;23:101645. 
62. Ehrlich A, Uhl S, Ioannidis K, Hofree M, tenOever BR, Nahmias Y. The SARS-CoV-2 Transcriptional Metabolic Signature in Lung Epithelium. SSRN Electron J [Internet]. 2020 [cited 2021 Feb 10]; Available from: https://www.ssrn.com/abstract=3650499
63. Bae J-S, Park J-M, Lee J, Oh B-C, Jang S-H, Lee YB, et al. Amelioration of non-alcoholic fatty liver disease with NPC1L1-targeted IgY or n-3 polyunsaturated fatty acids in mice. Metabolism. 2017;66:32–44. 
64. Grant WB, Lahore H, McDonnell SL, Baggerly CA, French CB, Aliano JL, et al. Evidence that Vitamin D Supplementation Could Reduce Risk of Influenza and COVID-19 Infections and Deaths. Nutrients. 2020;12. 
65. Sharifi A, Vahedi H, Nedjat S, Rafiei H, Hosseinzadeh-Attar MJ. Effect of single-dose injection of vitamin D on immune cytokines in ulcerative colitis patients: a randomized placebo-controlled trial. APMIS Acta Pathol Microbiol Immunol Scand. 2019;127:681–7. 
66. Sajuthi SP, DeFord P, Jackson ND, Montgomery MT, Everman JL, Rios CL, et al. Type 2 and interferon inflammation strongly regulate SARS-CoV-2 related gene expression in the airway epithelium. BioRxiv Prepr Serv Biol. 2020; 


