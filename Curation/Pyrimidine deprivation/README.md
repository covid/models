# Pyrimidine synthesis and deprivation pathway

## Map description
We present the pyrimidine synthesis pathway that has effects potentially both on viral DNA and RNA synthesis. Pyrimidine deprivation is a host targeted antiviral defense mechanism, which blocks the viral replication but is not exposed to viral mutation induced loss of efficacy.

Existing drugs that act on this pathway and on the enzyme DHODH (dihydroorotate dehydrogenase) in particular (like the drug teriflunomide (PMID:25342978)) but also many experimental therapeutics under development, like P1788 (PMID:31740051) or S312 and S314 (doi: https://doi.org/10.1101/2020.03.11.983056)  amplify cellular response to both type-I and type II interferons. It appears that components of the DNA damage response connect the inhibition of pyrimidine biosynthesis by terifluomide (P1788 and others) to the interferon signaling pathway probably via STING induced TBK1 activation that finally amplifies interferon response to viral infection.

## Cross-talk with other pathways (in the map, and in general)

Since the pyrimidine deprivation finally leads to amplified immune response via for example IFN-gamma production, it is linked most intricately to the interferon pathways. However, other related pathways are also clearly impacted. (below)

### Downstream
Interferon pathways
HcoVs interferon induction
RTC and transcription
Virus replication cycle

## Creators
- [Zsolt Bocskei](https://fairdomhub.org/people/1657)
- [Franck Augé](https://fairdomhub.org/people/1665)
- [Anna Niarakis](https://fairdomhub.org/people/1554)

## Model at FairdomHub (https://fairdomhub.org/models/752)
