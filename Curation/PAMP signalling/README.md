# COVID-19 PAMP Signaling

## Map description
The innate immune system can detect specific molecular patterns of microbes, the so-called pathogen-associated molecular patterns (PAMPs), through Pattern Recognition Receptors (PRRs). These PRRs include the big family of Toll-like receptors (TLRs) and other transmembrane but also intracellular receptors, each specified on particular PAMPs. Detection of Sars-Cov-2 is mediated through TLR3, TLR7, TLR8, RIG-I (gene: ddx58), MDA5 (gene: ifih1) and protein kinase R (PKR, gene: eif2ak2) that recognize double-stranded and single-stranded RNA  either in the endosome during endocytosis of the virus particle or in the cytoplasm during the viral replication. These receptors activate different downstream pathways that can be classified into the MYD88-dependent (by TLR7 and TLR8), MYD88-independent (by TLR3), and MAVS-mediated (RIG-I and MAD5) pathways. They all lead to the activation of transcription factors such as AP1, NFKB, IRF3, and IRF7. Especially IRF3 and IRF7 are responsible for the transcription of antiviral proteins, in particular, interferon alpha and beta [1], [2].

Viral proteins that interfere with these pathways are included in the map. Sars-Cov-2 has been shown to reduce the production of type I interferons to evade the immune response [3]. The detailed mechanism is not clear yet, however, an inhibitory effect of the Sars-Cov-1 M protein (with 96.4% similarity to the Sars-Cov-2 M protein [4]) on the TRAF3:TANK:TBK1/IKKε axis and, therefore, IRF3 activation has been reported in 2009 [5]. Evidence of effects on the NFKB axis, however, is somewhat contradicting. A suppression of NFKB and COX2 transcription by the M protein has been reported as well as an NFKB activation by the N protein (with 94.3% similarity to the Sars-Cov-2 N protein [4]), both for Sars-Cov-1 [6], [7].

## Cross-talk with other pathways (in the map, and in general)

The map contains the initial recognition process of the viral particle by the innate immune system and the viral mechanisms to evade the immune response. It provides the connection between the virus entry (detecting the endosomal viral patterns), the virus replication cycle (detection cytoplasmatic viral patterns), and the effector pathways of pro-inflammatory cytokines, especially of the interferon type I class. Latter seems to play a crucial but complex role in the Covid-19 pathology. In vitro and in vivo (the latter only for Sars-Cov-1, yet) administration of Interferons (as antiviral cytokines in the immune defense) has resulted in a dose-dependent reduction of the virus titer [8], [9]. Contradictory, multiple studies have shown that ACE2, one of the main entry proteins for Sars-Cov-2 belongs to the interferon-stimulated genes (ISGs), providing arguments for a positive effect of interferons on the virus replication [10], [11]. The molecular pathways behind the Covid-19 pathology are complex and are probably the result of very specific regulatory mechanisms causing the virus’ infectiousness. Clarifying the mechanisms of how Sars-Cov-2 modulates the innate immune defense in its favor to either evade recognition or, on the other hand, stimulate the immune response to amplify its replication is crucial to understand the virus pathology.

## References

- [1]	T. H. Mogensen, “Pathogen recognition and inflammatory signaling in innate immune defenses,” Clin. Microbiol. Rev., vol. 22, no. 2, pp. 240–273, 2009, doi: 10.1128/CMR.00046-08.
- [2]	O. Takeuchi and S. Akira, “Pattern Recognition Receptors and Inflammation,” Cell, vol. 140, no. 6. Cell, pp. 805–820, Mar. 2010, doi: 10.1016/j.cell.2010.01.022.
- [3]	H. Chu, J. F.-W. Chan, Y. Wang, et al., “Comparative replication and immune activation profiles of SARS-CoV-2 and SARS-CoV in human lungs: an ex vivo study with implications for the pathogenesis of COVID-19,” Clin. Infect. Dis., p. ciaa410, Apr. 2020, doi: 10.1093/cid/ciaa410.
- [4]	D. E. Gordon, G. M. Jang, M. Bouhaddou, et al., “A SARS-CoV-2 protein interaction map reveals targets for drug repurposing,” Nature, pp. 1–13, Apr. 2020, doi: 10.1038/s41586-020-2286-9.
- [5]	K. L. Siu, K. H. Kok, M. H. J. Ng, et al., “Severe acute respiratory syndrome coronavirus M protein inhibits type I interferon production by impeding theformation of TRAF3·TANK·TBK1/IKKε complex,” J. Biol. Chem., vol. 284, no. 24, pp. 16202–16209, Jun. 2009, doi: 10.1074/jbc.M109.008227.
- [6]	X. Fang, J. Gao, H. Zheng, et al., “The membrane protein of SARS-CoV suppresses NF-κB activation,” J. Med. Virol., vol. 79, no. 10, pp. 1431–1439, Oct. 2007, doi: 10.1002/jmv.20953.
- [7]	Q. J. Liao, L. B. Ye, K. A. Timani, et al., “Activation of NF-κB by the full-length nucleocapsid protein of the SARS coronavirus,” Acta Biochim. Biophys. Sin. (Shanghai)., vol. 37, no. 9, pp. 607–612, Sep. 2005, doi: 10.1111/j.1745-7270.2005.00082.x.
- [8]	E. Mantlo, N. Bukreyeva, J. Maruyama, et al., “Antiviral activities of type I interferons to SARS-CoV-2 infection,” Antiviral Res., vol. 179, p. 104811, Jul. 2020, doi: 10.1016/j.antiviral.2020.104811.
- [9]	T. Kuri, X. Zhang, M. Habjan, et al., “Interferon priming enables cells to partially overturn the SARS coronavirus-induced block in innate immune activation,” J. Gen. Virol., vol. 90, no. 11, pp. 2686–2694, 2009, doi: 10.1099/vir.0.013599-0.
- [10]	C. G. K. Ziegler, S. J. Allon, S. K. Nyquist, et al., “SARS-CoV-2 Receptor ACE2 Is an Interferon-Stimulated Gene in Human Airway Epithelial Cells and Is Detected in Specific Cell Subsets across Tissues,” Cell, vol. 181, no. 5, pp. 1016-1035.e19, May 2020, doi: 10.1016/j.cell.2020.04.035.
- [11]	S. Su and S. Jiang, “A suspicious role of interferon in the pathogenesis of SARS-CoV-2 by enhancing expression of ACE2,” Signal Transduct. Target. Ther., vol. 5, no. 1, p. 71, May 2020, doi: 10.1038/s41392-020-0185-z.

## Creators
- [Matti Hoch](https://fairdomhub.org/people/1520)


## Contributors and reviewers
- [Shailendra Gupta](https://fairdomhub.org/people/1641)
- **Suchi Smita**
- [Julia Scheel](https://fairdomhub.org/people/1686)

## Model at FairdomHub (https://fairdomhub.org/models/735)
