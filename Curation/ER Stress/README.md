# Endoplasmic Reticulum Stress

## Map description

The endoplasmic reticulum (ER), is a vast organelle with many functions for cell homeostasis, such as Ca2+ storage, synthesis and folding of proteins as well as carbohydrate and lipid metabolism. Many conditions, such as oxidative stress, altered Ca2+ homeostasis, fails on protein folding can cause the accumulation of unfolded or misfolded proteins in the ER, leading to the stress of this organelle. The ER has many pathways to resolve this stress, however, when it fails to restore its function can trigger cell apoptosis [PMID 25387057], [PMID 25656104].

The expression of some HCoV proteins during infection, specially the S glycoprotein, might induce the activation of the ER stress in the host’s cells [PMID 22915798]. The unfolded protein response pathways are key to assure the ER homeostasis, these pathways are activated by the protein kinase RNA-activated (PKR)-like ER protein kinase (PERK), inositol-requiring enzyme 1 (IRE1), and activating transcription factor 6 (ATF6) [PMID 31226023]. During SARS-Cov infection, it has been proved the activation of the PERK [PMID 19109397], IRE1 [PMID 22028656] and, in a less studied and indirect manner, ATF6 pathways [PMID 19304306].

This map depicts the activation of the main UPR actors (ATF6. IRE1 and PERK) upon unfolded protein accumulation, and their role of this response to Ca2+ release into the cytoplasm as well as the activation of molecules that can lead to apoptosis and cell death.


## Cross-talk with other pathways (in the map, and in general)

This map can be linked to the ER quality control map as an upstream pathway and as downstream pathway the apoptosis map and also calcium homeostasis.

## Creators
- [Cristobal Monraz-Gomez](https://fairdomhub.org/people/1582)
- [Inna Kuperstein](https://fairdomhub.org/people/1548)
- [Barbara Brauner](https://fairdomhub.org/people/1693)

## Model at FairdomHub (https://fairdomhub.org/models/719)
