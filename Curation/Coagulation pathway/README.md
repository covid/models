# SARS-CoV-2 coagulation pathway

## Map description

Thrombotic complications and coagulopathy frequently occur in COVID-19 [PMID: 32552865]. Recently, a high incidence of venous thromboembolism was found by consecutive autopsy findings noting frequent deep vein thrombosis in 7 of 12 COVID-19 cases; pulmonary embolism was the direct cause of death in 4 patients (33%) [PMID:32374815]. Characteristics of COVID-19-associated coagulopathy are increased D-dimer and fibrinogen levels but initially minimal abnormalities in prothrombin time and platelet count. Moreover, hyperinflammation also induces endothelial dysfunction leading to microvascular thrombosis with further organ damage (PMID:32348783). There are multiple connections between coagulopathy and pathways that were found to be involved in SARS-CoV-2 infections. 1.) The increased level of Angiotensin II after virus infection, which indicates the renin-angiotensin system imbalance, mediates activation of different components of the coagulation cascade that leads to microvascular thrombosis (PMID:24495185). 2.) The COVID-19 non‐survivors revealed significantly higher D‐dimer and fibrin degradation product levels, longer prothrombin time and activated partial thromboplastin time compared to survivors on admission (PMID:32073213). 3.) The crosstalk of the SARS-CoV-2 virus with the kallikrein-kinin system and the complement cascade play a role in the development of microvascular thrombosis. 4.) SARS-CoV-2 spike glycoprotein was shown to co-localize with C4d and C5b-9 in the interalveolar septa and the cutaneous microvasculature of 2 Covid-19 cases examined and the C4d-deposits found in COVID-19 patients may be correlated with septal capillary necrosis (PMID:32299776). 5.) Inflammatory biomarkers (IL1beta, IL-6, IL8, IL-10), coagulation factors (C-reactive protein, Factor VIII, VWF, protein C) and complementation components (C4d, C5b-9) were highly upregulated in patients with severe and fatal disease (PMID:32504360, PMID:32286245).

## Cross-talk with other pathways (in the map, and in general)

Our pathway model shows how the proteins with altered expression level can be integrated in a tightly interconnected network. This network intends to identify potential regulatory mechanisms between of five thrombosis related pathways such as coagulation cascade, renin-angiotensin system, complement component cascade, kallikrein-kinin system and hyperinflammation for COVID-19 patients with severe immunothrombosis. SARS-CoV-2 infection coagulation pathway is interlinked with the “SPIKE-ACE2-RAS pathway” map via ACE2 and pro-thrombotic function of Angiotensin II. SARS-CoV-2 infection triggers hyperinflammation that leads detrimental hypercoagulability and immunothrombosis. Therefore, this pathway also be linked to “SARS-CoV-2 NRLP3 Inflammasome (WP4876)” via “cytokine storm” and “PAMP signaling” via “transcription of pro-inflammatory factors”.

## Creators
- [Goar Frishman](https://fairdomhub.org/people/1696)
- [Gisela Fobo](https://fairdomhub.org/people/1695)
- [Corinna Montrone](https://fairdomhub.org/people/1694)

## Contributors and reviewers
- [Andreas Ruepp](https://fairdomhub.org/people/1692)

## Model at FairdomHub (https://fairdomhub.org/models/749)
