# Renin-angiotensin system

## Map description
SARS-CoV-2 uses ACE2 (angiotensin-converting enzyme 2) as receptor to enter host cells. ACE2 is a regulator of the renin-angiotensin system (RAS) and is widely expressed in the organs affected in COVID-19 patients like lungs, kidneys, cardiovascular system, gut, and central nervous system.

An important function of ACE2 is the conversion of angiotensin II (AngII) to angiotensin 1-7 (Ang1-7). These two angiotensins trigger counter-regulatory arms of the RAS. The signaling via AngII and its receptor AGTR1 induces deleterious effects (e.g. vasoconstriction, inflammation, thrombosis), while the signaling via Ang1-7 and its receptor MAS1 attenuates these effects. In SARS-CoV2 patients AngII levels are elevated, so that the balance is shifted towards the harmful signaling via AngII/AGTR1. RAS, which includes not only AngII, Ang1-7, AGTR1 and MAS1 but more angiotensin forms and receptors, is tightly regulated and it is influenced by factors that are known to increase the risk to develop severe forms of COVID-19, like hypertension, male sex, or aging.

RAS-inhibiting drugs, ACE inhibitors (ACEi) and Angiotensin II type 1 receptor blockers (ARB), are commonly used in the treatment of hypertension and chronic kidney disease. The role of the RAS in the pathogenesis of COVID-19 and the effects of ACEis and ARBs on the risk of COVID-19 infection and disease progression are currently under investigation [PMID 31427727; PMID 32264791; PMID 32336612].

Similarly, the role of activated vitamin D (Calcitriol) as a negative regulator of renin transcription is currently being studied as a potential factor that may influence the severity of COVID-19 [PMID 32252338]. Further known small molecule compounds and drugs that influence the activity of key enzymes and receptors in the RAS have been annotated in the map.

## Cross-talk with other pathways (in the map, and in general)

Describe the role of your pathway/diagram in a bigger context. Provide a short list of upstream and downstream pathways, both in the map and in general, that interact with the mechanisms you have drawn. You can check the current content of the COVID-19 Disease Map here: [https://covid19map.elixir-luxembourg.org](https://covid19map.elixir-luxembourg.org).

The pathway is interlinked with the “Virus replication cycle” map via ACE2 as the main receptor for viral entry into host cells, and viral replication can be regarded as a downstream pathway after viral entry. Moreover, the pathway has generic downstream mechanisms (not within the map), which include inflammation, oxidative stress, vasoconstriction, fibrosis, pulmonary edema, and coagulation/thrombosis (downstream of AGTR1, counter-acted by MAS1 signaling), decreased neurodegeneration and improved cognition (downstream of LNPEP).

## Creators
- [Enrico Glaab](https://fairdomhub.org/people/1674)
- [Gisela Fobo](https://fairdomhub.org/people/1695)
- [Corinna Montrone](https://fairdomhub.org/people/1694)

## Contributors and reviewers
[Andreas Ruepp](https://fairdomhub.org/people/1692)
[Marek Ostaszewski](https://fairdomhub.org/people/665)


## Model at FairdomHub (https://fairdomhub.org/models/709)
