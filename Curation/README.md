# Diagrams curated bu the [COVID-19 Diseas Maps Community](https://fairdomhub.org/projects/190)

See specific directories for the diagrams of respective pathways. README files in the directories describe the scope and contents of the curated diagrams.

Diagrams are in [CellDesigner SBML format](www.celldesigner.org) or in [SBGNML](https://sbgn.github.io), the links provide access to preferred editors of these pathways.

**Curation roadmap.md** describes the areas of the planned curation, higlighting important aspects of COVID-19 still lacking molecular mechanisms in the literature.
