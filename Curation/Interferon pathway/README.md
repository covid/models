# Innate Immunity and Type 1 Interferon pathway

## Map description

### Abbreviations:
PMPs- pathogen-associated molecular patterns,
PRRs-pattern recognition receptors,
IRF3-interferon regulatory factor 3,
NF-κB- nuclear factor kappa-light-chain-enhancer of activated B cells,
AP-1  activator protein 1,
IFN-I  type I interferons,
IFNAR IFN-α/β receptor,  
ISGs interferon-stimulated genes,
JAK1- Janus kinase 1,
STAT- signal transducer and activator of transcription 1,
TLR Toll-like receptor,
RLRs RIG-I-like receptors,

Innate immunity plays an important role in the detection and restriction of pathogens and also activation of the adaptive immune response. Effective activation of innate immunity relies on the:
Recognition pathogen-associated molecular patterns (PAMPs) by pattern recognition receptors (PRRs)
PRRs recruit adaptor proteins, which initiate complicated signalling pathways involving multiple kinases.
Activation of critical transcription factors including interferon regulatory factor 3 (IRF3), nuclear factor kappa-light-chain-enhancer of activated B cells (NF-κB), and activator protein 1 (AP-1).
These factors promote the production of type I interferons (IFN-I), which are released and act on neighbouring cells by binding to IFN-α/β receptor (IFNAR)
The antiviral activity of IFN-I is mediated by the induction of numerous interferon-stimulated genes (ISGs), which antagonize viral replication by various mechanisms.

### How they are related to the COVID:

While mild HCoVs typically induced a high level of IFN-I production (Dendritic Cells, MeSH ID: D003713) [ref 1] [PMID: 22553325].  SARS-COVID proteins were shown to utilize mechanisms to suppress the activation of host innate immune response. Several structural proteins (M and N), nonstructural proteins (nsp1 and nsp3), and accessory proteins of SARS-COVID were identified as interferon antagonists [refs 2–5] [PMID: 31226023, PMID: 32201497, PMID: 28933406, PMID: 24995382].

### What are the key molecular mechanisms:

1. Binding the IFN1 to IFNAR triggers the JAK1/STAT pathways which fall into the activation of the immune response 2 [PMID: 31226023].

2. PAMPs trigger the activation of TLR7 (TLRs) and RIG-I-like receptors (RLRs) [ref 4] [PMID: 28933406] which recruit adaptor proteins, initiating signalling pathways involving multiple kinases. This cascade leads to the activation of crucial transcription factors such as IRF3, NF-κB, and AP-1. These factors promote the production of IFN-I, which are released and act on neighbouring cells by binding to the IFN-α/β receptor (IFNAR) [ref 4] [PMID: 28933406].

3. The antiviral activity of IFN-I is mediated by the induction of ISGs. Also, cytokines and chemokines are induced to activate an inflammatory response, which is also sometimes responsible for extensive tissue damage and other immunopathies associated with Human-COVID infection [ref 6] [PMID: 19430490].

## Cross-talk with other pathways (in the map, and in general)

Since PAMPs are the trigger for PRRs, this map may have cross-talk with “PAMP signalling”, “HCoVs Ifn induction (WP4880) and HCoVs Type I Ifn signaling (WP4868)”, and “Orf3a interactions” that all of them share TLR7 (TLRs).
All of the pathways mentioned plus “Pyrimidine deprivation” can have cross-talk with our map by IFN (IFNB1, IFNA1) which is related to IFN pathways.
Downstream of our map (immune response phenotype) and (inflammation phenotype) may have cross-talk with “Pyrimidine deprivation” by its induction_of_cellular_immune_response_phenotype and also “HCoVs Ifn induction (WP4880) and HCoVs Type I Ifn signaling (WP4868)”, “JNK pathway” by Innate_immunity_phenotype. With “SARS-CoV-2 NLRP3 Inflammasome (WP4876)” by NLRP3_inflammasome_complex

Bibliography
1.	Mesel-Lemoine, M. et al. A human coronavirus responsible for the common cold massively kills dendritic cells but not monocytes. J. Virol. 86, 7577–7587 (2012).
2.	Fung, T. S. & Liu, D. X. Human Coronavirus: Host-Pathogen Interaction. Annu. Rev. Microbiol. 73, 529–557 (2019).
3.	Fung, T. S. & Liu, D. X. Post-translational modifications of coronavirus proteins: roles and function. Future Virol. 13, 405–430 (2018).
4.	Lim, Y. X., Ng, Y. L., Tam, J. P. & Liu, D. X. Human Coronaviruses: A Review of Virus-Host Interactions. Diseases 4, (2016).
5.	Liu, D. X., Fung, T. S., Chong, K. K.-L., Shukla, A. & Hilgenfeld, R. Accessory proteins of SARS-CoV and other coronaviruses. Antiviral Res. 109, 97–109 (2014).
6.	Perlman, S. & Netland, J. Coronaviruses post-SARS: update on replication and pathogenesis. Nat. Rev. Microbiol. 7, 439–450 (2009).


## Creators (alphabetically)
- [Sara Aghamiri](https://fairdomhub.org/people/1659)
- [Vidisha Singh](https://fairdomhub.org/people/1658)
- [Anna Niarakis](https://fairdomhub.org/people/1554)


## Contributors and reviewers
- [Marek Ostaszewski](https://fairdomhub.org/people/665)

## Model at FairdomHub (https://fairdomhub.org/models/713)
