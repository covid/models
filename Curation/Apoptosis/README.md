# Apoptosis

## Map description

Innate cell death machinery also called Apoptosis or programmed cell death is one of the many host cellular responses triggered by virus-host interaction upon viral infection. The induction of apoptosis is considered to be a host defence response as the early death of the virus-infected cells could prevent viral replication. Many viruses have developed a countermeasure(s) that effectively blocks or delays cell death by expressing anti-apoptotic proteins to maximize the production of viral progeny (12441076). Apoptosis induction at the end of the viral replication cycle might assist in viral dissemination while attenuating an inflammatory response.

Apoptosis can be activated through both intrinsic and extrinsic pathways in the cell.
Intrinsic pathway includes proteins of the BCL2 family which could be both pro-apoptotic and anti-apoptotic. Pro-apoptotic proteins like BAX and BAK1 are channel-forming proteins that increase the mitochondrial outer membrane permeability (MOMP) while Bcl2, Bcl-xL, and Mcl-1 are anti-apoptotic factors that inhibit this process. Extrinsic pathways include the binding of the death ligands [such as FasL and tumour necrosis factor-α (TNF-α)] to the cell surface death receptors (such as Fas and TNF receptor 1). This initiates death-inducing signalling complex and activation of caspase 8, which either directly activates effector caspases or engages in cross-talk with the intrinsic pathway by activating the BH3-only protein Bid. Bid protein activates BAX proteins in the mitochondria which eventually releases Cytochrome C in the cytoplasm where it interacts with APAF1 protein and procaspase 9 resulting in the formation of Apoptosome complex (31226023).
Apoptosis induced by HCoV infection in different cell/tissue types:
HCoV-OC43 induced apoptosis in neuronal cells (22013052)
SARS-CoV infected lung, spleen, and thyroid tissues (19109397)
MERS-CoV induced apoptosis in primary T lymphocytes (26203058)
HCoV-229E infection also causes massive cell death in dendritic cells (22553325)
SARS-CoV-2 (Covid-19) induces lymphocyte apoptosis via enhancing Fas signaling (https://doi.org/10.1101/2020.03.27.20045427)


## Cross-talk with other pathways (in the map, and in general)

Apoptosis is regulated by Upstream Pathways FASL and TNF and the downstream pathways include AKT and MAPKs. AKT and MAPKs operate individually in the regulation of Apoptosis and are not activated by the upstream TNF and FASL pathway.
 Cross talks with other pathways in the COVID-19 disease map:
ER stress via BCL2 and BAX protein
JNK pathway via BCL2 protein
HCoVs ER stress (WP4869)	via MAPKs (however, Apoptosis pathway is regulated by MAPK14 while in HCoVs ER stress (WP4869) pathway, Apoptosis is regulated by a complex of MAPKs (MAPK8,9,10)

## Creators
- [Vidisha Singh](https://fairdomhub.org/people/1658)
- [Anna Niarakis](https://fairdomhub.org/people/1554)
- [Sara Ahgamiri](https://fairdomhub.org/people/1659)

## Contributors and reviewers
- [Marek Ostaszewski](https://fairdomhub.org/people/665)

## Model at FairdomHub (https://fairdomhub.org/models/712)
