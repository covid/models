# JNK pathway activation during SARS-CoV infection

## Map description
During activation JNK translocates to the nucleus to phosphorylate ATF2 and c-Jun [PMID 1749429] which in turn dimerizes with other proteins to form the AP-1 complex [PMID 15564374]. Additionally, JNK translocates to the mitochondria to phosphorylate Bcl2, promoting stress-induced apoptosis [PMID: 10567572].
Vero E6 cells infected with SARS-CoV revealed phosphorylation of JNK and its upstream kinases MAP2K4 and MAP2K7 (MKK4/MKK7) [PMID: 15916886]. Here, overexpression of SARS-CoV 3a and 7a protein increased JNK [PMID: 17141229], while SARS-CoV 3b protein induced JNK/c-Jun/AP-1 activation [PMID: 21561061], and SARS-CoV S protein induced the activation of protein kinase epsilon via JNK activation [PMID: 17267381]. Expression of SARS-CoV N protein is associated with the downregulation of prosurvival factors and apoptosis induction in COS-1 cells, possibly mediated by JNK activation [PMID: 15294014], and apoptosis induced by SARS-CoV 6 and 7a protein in Vero E6 and COS-7 cells were blocked by a JNK inhibitor [PMID: 18708124]. Therefore, JNK is a good candidate acting as a pro-apoptotic protein during SARS-CoV infection. On the other hand, a prosurvival function of JNK was suggested after Vero E6 cells were treated with JNK inhibitor that abolished persistent infection of SARS-CoV [PMID: 15916886 ], apoptosis induced by overexpression of SARS-CoV N or 6 or 7a was JNK dependent [PMID: 28933406], and activation of JNK promoted IBV-induced apoptosis [PMID: 25142592] [PMID: 29238080]. Presumably JNK might be proapoptotic during initial SARS-CoV infection but later switched to a prosurvival role in persistently infected cells [PMID: 31226023] [PMID: 28933406].

## Cross-talk with other pathways (in the map, and in general)

The MAPKs are activated in response to environmental stresses including oxidative stress, DNA damage, cancer development and viral infections. The c-Jun N-terminal kinase (JNK) belong to the MAPK pathways [PMID: 23954936]. Phosphorylation of JNK and its upstream kinases, (MAP2K4/MAP2K7 or MKK4/MKK7), was detected in SARS-CoV-infected cells [PMID: 15916886].


## Creators (alphabetically)
- [Daniela Börnigen](https://fairdomhub.org/people/1583)

## Model at FairdomHub (https://fairdomhub.org/models/735)
