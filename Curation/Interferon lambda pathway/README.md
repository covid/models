# IFN-lambda map

## Map description
The Interferon-lambda (IFNL) map describes the action of the drug candidate IFNL on intra- and intercellular signal transduction and consists of two parts. The upper part of the map describes the binding of IFNL to its receptor, IFNLR, and the subsequent signaling cascade. IFNLR is preferentially expressed on epithelial cells [DOI 10.1016/j.immuni.2015.07.001]. JAK-STAT-signaling leads to the induction of Interferon Stimulated Genes (ISGs), which encode for antiviral proteins [DOI 10.1038/s41564-019-0421-x].
The bottom part describes the induction of transcription of IFNL in innate immune cells or infected lung epithelial cells. Intracellular RIG-1-like-receptor recognize viral RNA. This leads to activation of IRFs via the mitochondrial / peroxisomal MAVS pathway, which in turn leads to transcription of IFN-III genes [DOI 10.1038/ni.2924].
The interactions of viral proteins with the IFNL pathway are mapped where confirmed for SARS-CoV-2 [DOI 10.1101/2020.05.18.102467] or where amino acid sequence homology with SARS-CoV is big enough to assume the same mechanism for SARS-CoV-2 [DOI 10.1016/j.chom.2020.05.008].

## Cross-talk with other pathways (in the map, and in general)
IFNL may be the main IFN produced in lung cells during viral infection based on experiments with other respiratory viruses [DOI 10.1016/j.immuni.2015.07.001]. They may play a role in the so-called “cytokine storm” observed in some Covid-19 patients, as IFNL act more locally and lack the strong pro-inflammatory response associated with IFN type 1 [DOI 10.15252/emmm.202012465].

## Related pathways:
- PAMP signaling
- Interferon 1 pathway
- Virus replication cycle
- HCoVs IFN induction (WP4880)

## Creators (alphabetically)
- [Marta Conti](https://fairdomhub.org/people/1807)
- [Marius Rameil](https://fairdomhub.org/people/1802)
- [Vanessa Nakonecnij](https://fairdomhub.org/people/1801)

## Contributors and reviewers
- [Marta Conti](https://fairdomhub.org/people/1807)
- [Marius Rameil](https://fairdomhub.org/people/1802)
- [Vanessa Nakonecnij](https://fairdomhub.org/people/1801)
- [Jakob Vanhoefer](https://fairdomhub.org/people/1800)
- [Jan Hasenauer](https://fairdomhub.org/people/842)

## Model at FairdomHub (https://fairdomhub.org/models/750)
