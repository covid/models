# Orf10-Cul2 map

## Map description
The Orf10-Cul2 map describes the interaction of Orf10 with the ubiquitin-proteasome system. The ubiquitin-proteasome system regulates the intracellular sorting and degradation of proteins via ubiquitination, an enzymatic post-translational modification in which a ubiquitin protein is attached to a substrate protein. Polyubiquitination marks substrates for degradation by the 26S proteasome [DOI 10.1038/35056583]. Orf10 interacts with the ubiquitin-proteasome system by binding to different members of the Cul2-complex, a ubiquitin-ligase (E3) [DOI 10.1038/s41586-020-2286-9]. A particularly strong interaction was observed for the substrate adaptor Zyg11b [DOI 10.1038/s41586-020-2286-9], a RING finger protein. Under normal conditions, Zyg11b binds protein substrates with N-terminal glycine, but this binding might be altered by Orf10.
The map describes the ubiquitin-activating enzymes (E1s), the ubiquitin-conjugating enzymes (E2s), the Cul2-ubiquitin ligase (E3) and the 26S proteasome. The formation of the Cul2-complex and its interaction with Orf10 is included.

## Cross-talk with other pathways (in the map, and in general)
The ubiquitin-proteasome system is a key regulator of protein degradation and influences the levels of various proteins within the cell. This results in the modulation of cellular processes, including cell cycle progression and apoptosis [DOI: 10.1128/JVI.00485-10]. Furthermore, it has been shown that the ubiquitin-proteasome system plays an important role during various stages of the infection cycle of coronaviruses [DOI: 10.1128/JVI.00485-10]. RNA synthesis is modulated [DOI: 10.1128/JVI.00485-10] and antigen presentation on MHC class I molecules depends on proteasomal function [DOI 10.1681/ASN.2006010083].

## Creators
* [Jan Hasenauer](https://fairdomhub.org/people/842)
* [Leonard Schmiester](https://fairdomhub.org/people/1799)

## Contributors and reviewers
* **Paul Stapor**

## Model at FairdomHub (https://fairdomhub.org/models/702)
